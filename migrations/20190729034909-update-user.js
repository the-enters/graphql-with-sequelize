'use strict';

const tableName = "users";
module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(tableName, 'role', {
      type: Sequelize.INTEGER,
      after: "type",
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(tableName, 'role');
  }
};