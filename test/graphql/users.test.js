import * as authHelper from "../helper/authHelper";

describe('Users:', async () => {

  it('should be gotten by admin.', async () => {
    const token = await authHelper.loginUserAndGetToken(testUserOfAdmin);
    const access_token = token.access_token;
    expect(access_token).not.to.be.undefined;
    expect(access_token).not.to.be.null;

    try {
      const res = await request
        .post("/api")
        .set({
          "Accept": "application/json",
          "Authorization": "Bearer " + access_token,
        })
        .send({
          query: `
          query {
            users {
              email
              name
            }
          }`
        })
        .expect(200);

      try {
        const data = res.body.data.users;
        expect(data.length).to.be.at.least(1);
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }

  });

  it('should be gotten by team manager.', async () => {
    const token = await authHelper.loginUserAndGetToken(testUserOfTeamManager);
    const access_token = token.access_token;
    expect(access_token).not.to.be.undefined;
    expect(access_token).not.to.be.null;

    try {
      const res = await request
        .post("/api")
        .set({
          "Accept": "application/json",
          "Authorization": "Bearer " + access_token,
        })
        .send({
          query: `
          query {
            users {
              email
              name
            }
          }`
        })
        .expect(200);

      try {
        const data = res.body.data.users;
        expect(data.length).to.be.at.least(1);
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }
  });

  it('should not be gotten by employee.', async () => {
    const token = await authHelper.loginUserAndGetToken(testUserOfEmployee);
    const access_token = token.access_token;
    expect(access_token).not.to.be.undefined;
    expect(access_token).not.to.be.null;

    try {
      const res = await request
        .post("/api")
        .set({
          "Accept": "application/json",
          "Authorization": "Bearer " + access_token,
        })
        .send({
          query: `
          query {
            users {
              email
              name
            }
          }`
        })
        .expect(200);

      try {
        expect(res.body.errors[0].message).to.be.eq("Not Authorised!");
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }
  });
});