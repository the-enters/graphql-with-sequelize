describe('Auth:', async () => {
  it('should be logged in as admin user.', async () => {
    try {
      const res = await request
        .post("/api")
        .set('Accept', 'application/json')
        .send({
          query: `
          mutation {
            loginUser(input: {
              id: "${testUserOfAdmin.id}",
              password: "${testUserOfAdmin.password}"
            }) {
              access_token
              refresh_token
            }
          }`
        })
        .expect(200);

      try {
        const data = res.body.data.loginUser;
        expect(data.access_token).to.be.not.null;
        expect(data.access_token).to.be.not.undefined;
        expect(data.refresh_token).to.be.not.null;
        expect(data.refresh_token).to.be.not.undefined;
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }
  });

  it('should be logged in as team manager user.', async () => {
    try {
      const res = await request
        .post("/api")
        .set('Accept', 'application/json')
        .send({
          query: `
          mutation {
            loginUser(input: {
              id: "${testUserOfTeamManager.id}",
              password: "${testUserOfTeamManager.password}"
            }) {
              access_token
              refresh_token
            }
          }`
        })
        .expect(200);

      try {
        const data = res.body.data.loginUser;
        expect(data.access_token).to.be.not.null;
        expect(data.access_token).to.be.not.undefined;
        expect(data.refresh_token).to.be.not.null;
        expect(data.refresh_token).to.be.not.undefined;
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }
  });

  it('should be logged in as employee user.', async () => {
    try {
      const res = await request
        .post("/api")
        .set('Accept', 'application/json')
        .send({
          query: `
          mutation {
            loginUser(input: {
              id: "${testUserOfEmployee.id}",
              password: "${testUserOfEmployee.password}"
            }) {
              access_token
              refresh_token
            }
          }`
        })
        .expect(200);

      try {
        const data = res.body.data.loginUser;
        expect(data.access_token).to.be.not.null;
        expect(data.access_token).to.be.not.undefined;
        expect(data.refresh_token).to.be.not.null;
        expect(data.refresh_token).to.be.not.undefined;
      } catch (e) {
        consoleError(res.body);
        throw e;
      }
    } catch (e) {
      throw e;
    }
  });

});