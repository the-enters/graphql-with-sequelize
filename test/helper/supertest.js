import App from "../../app";
import supertest from 'supertest';

const app = new App();
const server = app.getServer().createHttpServer({});

export const request = supertest(server);
