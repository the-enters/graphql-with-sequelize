import {loginUser} from "../../services/UserService";

export const loginUserAndGetToken = async (user) => {
  return await loginUser(null, {id: user.id, password: user.password});
};
