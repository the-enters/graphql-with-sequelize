'use strict';

import * as chai from 'chai';
import * as constants from '../common/constants';
import {request} from './helper/supertest';

before(() => {
  global.constants = constants;
  global.request = request;
  global.expect = chai.expect;
  global.should = chai.should();

  global.errMsg = constants.ERROR_MSG;

  global.testUserOfAdmin = {
    id: "user1",
    email: "user1@example.com",
    password: "123123",
  };

  global.testUserOfTeamManager = {
    id: "user26",
    email: "user26@example.com",
    password: "123123",
  };

  global.testUserOfEmployee = {
    id: "user75",
    email: "user75@example.com",
    password: "123123",
  };

  global.consoleError = (object) => {
    console.error(JSON.stringify(object, null, "  "));
  };
});

beforeEach(async () => {
});

afterEach(async () => {
});

after(async () => {
});
