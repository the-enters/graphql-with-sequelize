'use strict';

import models from "../models";
import {ROLE} from "../common/constants";
import Sequelize from "sequelize";

const Op = Sequelize.Op;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await models.User.update({
      role: ROLE.EMPLOYEE | ROLE.TEAM_MANAGER | ROLE.ADMIN,
    }, {
      where: {
        email: {
          [Op.lte]: "user20",
        }
      },
    });

    await models.User.update({
      role: ROLE.EMPLOYEE | ROLE.TEAM_MANAGER,
    }, {
      where: {
        email: {
          [Op.gt]: "user20",
          [Op.lte]: "user30",
        }
      },
    });

    await models.User.update({
      role: ROLE.EMPLOYEE,
    }, {
      where: {
        email: {
          [Op.gt]: "user30",
        }
      },
    });

  },

  down: async (queryInterface, Sequelize) => {
    await models.User.update({
      role: null,
    }, {where: {}});
  }
};
