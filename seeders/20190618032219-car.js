'use strict';

const util = require('../helper/util');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('cars', [
            {
                id: 1,
                brand_id: 1,
                name:"sonata",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 2,
                brand_id: 2,
                name:"k5",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 3,
                brand_id: 3,
                name: "e400",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 4,
                brand_id: 4,
                name: "q5",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 5,
                brand_id: 5,
                name: "x5",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 6,
                brand_id: 6,
                name: "sm3",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 7,
                brand_id: 6,
                name: "sm5",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('cars', null, {});
    }
};
