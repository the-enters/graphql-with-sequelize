'use strict';

import faker from 'faker';
import {getNow} from '../helper/util';
import * as bcrpt from "bcrypt";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const users = [];
    for (let i = 0; i < 100; i++) {
      users.push({
        id: `user${i + 1}`,
        email: `user${i + 1}@example.com`,
        password: await bcrpt.hash("123123", 1),
        name: faker.fake("{{name.lastName}}, {{name.firstName}}"),
        type: i % 2 ? "general" : "kakao",
        created_at: getNow(),
        updated_at: getNow(),
      });
    }

    return queryInterface.bulkInsert('users', users, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
