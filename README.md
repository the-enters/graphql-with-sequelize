# GraphQL with Sequelize

## Database Configuration
- You have to set config.json file. Please reference config.example.json file.
- /config/config.json
```$xslt
{
  "development": {
    "username": "username",
    "password": "password",
    "database": "ql_dev",
    "host": "localhost",
    "dialect": "mysql"
  },
  "test": {
    "username": "username",
    "password": "password",
    "database": "ql_test",
    "host": "localhost",
    "dialect": "mysql"
  },
  "production": {
    "username": "username",
    "password": "password",
    "database": "ql_prod",
    "host": "localhost",
    "dialect": "mysql"
  }
}
```

## DB Initialization of migrations and seeders for development
```bash
yarn dev_init
```

## Reset all migrations and seeders
```bash
yarn dev_reset
```

## Start GraphQL Server
```bash
yarn start
```

## Structures of files
    - config/config.json: Settings of database for dev, test, production
    - graphql/resolvers.js: graphql resolvers file
    - graphql/schema.graphql: graphql schema file
    
    - migrations: sequelize migration files
    - seeders: sequelize seeder files
    - models: db model files


## How to Use PlayGround
 - Write your config file.
 - Set all migration and seeders.
 - Start graphql server.
 - Access localhost:4000/play on your browser.
 - Practice the Query and Mutation in playground.


## Query, Mutation Structure Example
 - Write this query and mutation on your playground.

```$xslt
mutation{
  createUser(
    id: "id"
    email: "email"
    password: "password"
    name: "name"
    type: "type"
  ){
    id
    email
  }

  loginUser(
    id: "email"
    password: "password"
  )
}

query{
  users{
    id
    name
  }
}
```

## How to Login
 - If you want to login, you should call loginUser mutation.
 - You would received JWToken then you have to save JWToken.
 - When you call API, send request JWToken with header.