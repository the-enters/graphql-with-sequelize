import models from "../models";

export const getBrandsByJoinQuery = async () => {
  try {
    return await models.Brand.findAll({
      underscored: true,
      include: [{
        model: models.Car,
        as: "cars"
      }]
    });
  } catch (e) {
    return e;
  }
};
