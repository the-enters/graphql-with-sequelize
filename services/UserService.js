import models from "../models";
import jwtObj from "../config/jwt"
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import _ from 'lodash'
import {CreateTokens} from '../helper/auth';

//user validation way
export const isUser = async (parent, args, context) => {
  if (!context.user) {
    return false;
  } else {
    return true;
  }
}

export const getUsersByPagination = async (parent, args) =>{
  args.offset = args.offset * args.limit;
  const users = await models.User.findAll(args);

  return users;
};

export const createUser = async (parent, newUser) => {
    const user = newUser;
    user.password = await bcrypt.hash(user.password, 12);

    return models.User.create(user);
};

export const loginUser = async (parent, {id, password}) => {
    const user = await models.User.findOne({where: { id }});
    if (!user) {
        throw new Error('Not user with that id');
    }

    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
        throw new Error('incorrect password');
    }

    return CreateTokens(user);
};

export const updateUser = async (userForUpdate) => {
  try {
    await models.User.update(userForUpdate, {
      where: {
        id: userForUpdate.id
      }
    });
    return {id: userForUpdate.id};
  } catch (e) {
    return e;
  }
};

export const deleteUser = async (targetUser) => {
  try {
    const user = await models.User.findOne({
      where: {
        id: targetUser.id
      }
    });
    user.destroy();
    return {id: targetUser.id};
  } catch (e) {
    return e;
  }
};
