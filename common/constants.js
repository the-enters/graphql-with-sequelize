export const ROLE = {
  ADMIN: 1,
  TEAM_MANAGER: 2,
  EMPLOYEE: 4,
  CLIENT: 8,
};