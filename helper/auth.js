import jwt from 'jsonwebtoken';
import _ from 'lodash';
import models from "../models";


const secretKey = "createSecret";
const secretRefreshKey = "createRefreshSecret";

export const CreateTokens = async(user) =>{
    const createToken = jwt.sign(
        {
            user: _.pick(user, ['id', 'email', 'name', 'role'])
        },
        secretKey,
        {
            expiresIn: '1h'
        },
    );

    const createRefreshToken = jwt.sign(
        {
            user: _.pick(user, ['id', 'email', 'name', 'role'])
        },
        secretRefreshKey,
        {
            expiresIn: '14d'
        },
    );

    return {access_token: createToken, refresh_token: createRefreshToken};

};

export const DecodeToken = async (token) => {
    try {
        let decoded = await jwt.verify(token, secretKey);
        
        let user_id = decoded.user.id;
        let user = await models.User.findOne({where: { id: user_id }});

        return user;
    } catch (e) {
        // throw e;
        return false;
    }
};
