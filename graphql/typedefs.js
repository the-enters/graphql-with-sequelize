const path = require('path');
import { fileLoader, mergeTypes } from 'merge-graphql-schemas';

const typesArray = fileLoader(path.join(__dirname, '**/*.graphql'));

module.exports = mergeTypes(typesArray, {all: true});
