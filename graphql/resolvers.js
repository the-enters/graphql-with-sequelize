import {
  createUser,
  loginUser,
  updateUser,
  deleteUser,
  getUsersByPagination
} from "../services/UserService";

import {
  getBrandsByJoinQuery,
} from "../services/CarService";

import {resolver} from "graphql-sequelize";
import models from '../models';
import {isUser} from '../services/UserService';

const resolvers = {
    User: {},
    Brand: {
        cars: resolver(models.Brand.Car),
    },
    Car: {
        brand: resolver(models.Car.Brand),
    },
    Query: {
        isUser,
        users: resolver(models.User),
        user: resolver(models.User),
        getBrandsByJoinQuery: () => getBrandsByJoinQuery(),
        getUsersByPagination: (parent, args) => getUsersByPagination(parent, args),
        brands: resolver(models.Brand),
        cars: resolver(models.Car),
    },
    Mutation: {
        updateUser: (_, userForUpdate) => updateUser(userForUpdate),
        deleteUser: (_, targetUser) => deleteUser(targetUser),
        createUser: (parent, newUser) => createUser(parent, newUser),
        loginUser: (parent, data) => loginUser(parent, data.input),
    }
};

export default resolvers;