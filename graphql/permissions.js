import {rule, shield, and, or, not} from 'graphql-shield';
import {ROLE} from "../common/constants";

const hasRole = (allowedRoles, isForRefreshToken) => rule()(async (parent, args, context, info) => {
  if (!context.user) {
    return false;
  }
  for (let i in allowedRoles) {
    const allowedRole = allowedRoles[i];
    const hasRole = (context.user.role & allowedRole) > 0;
    if (hasRole) {
      return true;
    }
  }
  return false;
});

const permissions = shield({
  Query: {
    users: hasRole([ROLE.ADMIN, ROLE.TEAM_MANAGER]),
  },
  Mutation: {
  },
});

export default permissions;