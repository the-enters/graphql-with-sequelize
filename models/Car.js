import Sequelize, { Model } from 'sequelize';

class Car extends Model {
    static tableName = 'cars';

    static associate(models) {
        Car.Brand = Car.belongsTo(models.Brand, {
            as: 'brand',
        });
    }
}

export default (sequelize) => {
    Car.init({
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        brand_id: {
            type: Sequelize.STRING,
        },
        name: {
            type: Sequelize.STRING
        },
        created_at: Sequelize.STRING,
    }, {
        sequelize,
        tableName: Car.tableName,
        timestamps: true,
        underscored: true,
        paranoid: true,
    });

    return Car;
};
