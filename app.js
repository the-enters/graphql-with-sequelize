import { GraphQLServer } from "graphql-yoga";
import {resolvers, permissions} from "./graphql";
import typedefs from "./graphql/typedefs";
import { createContext, EXPECTED_OPTIONS_KEY } from 'dataloader-sequelize';
import models from './models';
import { DecodeToken } from './helper/auth'

class App {

  constructor() {
    this.server = new GraphQLServer({
      typeDefs: typedefs,
      resolvers,
      middlewares: [permissions],
      async context({request}) {
        // For each request, create a DataLoader context for Sequelize to use
        const dataloaderContext = createContext(models.sequelize);

        //Authorization by user JWT
        //if contain valid token, then returned user object
        let user = null;
        try {
          if (request.headers.authorization) {
            const token = request.headers.authorization.split(' ').pop();
            user = await DecodeToken(token);
          }
        } catch (e) {
          console.log("TCL: e", e);
          throw e;
        }

        return {
          [EXPECTED_OPTIONS_KEY]: dataloaderContext,
          user,
        };
      },
    });

    const options = {
      port: 4000,
      endpoint: '/api',
      subscriptions: '/subscriptions',
      playground: '/play',
    };
    this.server.options = Object.assign(this.server.options, options);
  }

  run() {
    this.server.start(
      options,
      () => console.log(`GraphQL Server Running on port ${options.port}`)
    );
  }

  getServer() {
    return this.server;
  }
}

export default App;
